package com.example.rxjavaapplication.Services;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class DataObservable<T> {
    //Data That will be Observed
    Observable <T> observable;

    //The Observer ON This Data
    DataObserver <T> observer;

    //the Thread Where Data will be Processed
    Scheduler workerThread= Schedulers.io();

    //The Thread Where Data Emitted after it's Processed
    Scheduler UIThread= AndroidSchedulers.mainThread();

    public DataObservable(Observable<T> observable) {
        this.observable = observable;
    }

    public DataObservable(Observable<T> observable,DataObserver<T> observer) {
        this.observable = observable;
        this.observer = observer;
    }

    public void start() {
                 observable
                .observeOn(UIThread)
                .subscribeOn(workerThread)
                .subscribe(observer);
    }
    public void setObserver(DataObserver<T> observer){
        this.observer = observer;
    }
    public void startObserver(DataObserver<T> observer){
        setObserver(observer);
        start();
    }
    public void destroy(){
        this.observer.destroy();
    }

}
