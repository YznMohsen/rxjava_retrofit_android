package com.example.rxjavaapplication.Services;

import com.example.rxjavaapplication.Models.User;


import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    public final String BASE_URL = "https://jsonplaceholder.typicode.com/";
    private final Retrofit.Builder retrofitBuilder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create());
    private final Retrofit retrofit = retrofitBuilder.build();
    private final userService userService = retrofit.create(userService.class);

    public DataObservable<User> getUser(int id) {
        return new DataObservable<>(userService.getUser(id));
    }

}
