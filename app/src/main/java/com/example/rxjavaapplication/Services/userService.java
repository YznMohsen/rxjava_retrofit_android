package com.example.rxjavaapplication.Services;

import com.example.rxjavaapplication.Models.User;
import io.reactivex.rxjava3.core.Flowable;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.internal.operators.observable.ObservableError;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface userService {

    @GET("users/{id}")
    Observable<User> getUser(@Path("id")int id );
}