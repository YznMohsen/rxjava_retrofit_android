package com.example.rxjavaapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.rxjavaapplication.Models.User;
import com.example.rxjavaapplication.Services.DataObservable;
import com.example.rxjavaapplication.Services.DataObserver;
import com.example.rxjavaapplication.Services.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.annotations.NonNull;

public class MainActivity extends AppCompatActivity {

    //Layout
    TextView textView;
    TextView textView2;

    //Api
    RetrofitClient retrofitClient;
    DataObservable<User> dataObservable;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initServices();
        initLayout();
        apiTester();

    }

    private void initServices() {
        retrofitClient = new RetrofitClient();
    }
    private void initLayout() {
        textView = findViewById(R.id.textView);
        textView2 = findViewById(R.id.textView2);
    }

    private void apiTester() {
        //Setting The Data That will be Observed
        dataObservable = retrofitClient.getUser(1);

        //Start Executing So background Thread get Data  , then we its Done will invoke this Method
        dataObservable.startObserver(new DataObserver<User>() {
            @Override
            public void onNext(@NonNull User user) {
                textView.setText(user.toString());
            }
        });

        //another Observer on same Data
        dataObservable.startObserver(new DataObserver<User>() {
            @Override
            public void onNext(@NonNull User user) {
                textView2.setText(user.toString());
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dataObservable.destroy();
    }

}