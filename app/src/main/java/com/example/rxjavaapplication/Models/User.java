package com.example.rxjavaapplication.Models;

import androidx.annotation.NonNull;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

public class User implements  Cloneable{


    @SerializedName("id")
    Integer id;

    @SerializedName("username")
    String username;


    @SerializedName("email")
    String email;

    @SerializedName("website")
    String website;

    public User(Integer id, String username, String email, String website) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.website = website;
    }

    public static User getFromJson(JsonElement jsonElement){
        return new GsonBuilder().create().fromJson(jsonElement, User.class);
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getWebsite() {
        return website;
    }

    @NonNull
    @Override
    public String toString() {
        return
                "id: "+id+
                "username: "+username+
                "email: "+email+
                "website: "+website;
    }
}
